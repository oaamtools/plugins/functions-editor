export async function init(pluginAPI) {
  await pluginAPI.loadStylesheets(["./css/oaam-functions-editor.css"]);

  pluginAPI.implement("xgee.models", {
    modelPath: pluginAPI.getPath() + "Functions.editorModel",
  });

  return true;
}

export var meta = {
  id: "editor.oaam.functions",
  description: "Editor for the Functions Layer of OAAM",
  author: "Matthias Brunner",
  version: "0.3.0",
  requires: ["ecore", "editor"],
};
